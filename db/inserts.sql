-- INSERT DE LA PAGINA DE INICIO
INSERT INTO Inicio (id, titulo, url) VALUES
(1, 'Bienvenidos al Grugo FSOCIETY', 'assets/images/logo.jpeg');

SELECT * FROM Inicio

-- INSERT DE LOS DATOS DE LOS INTEGRANTES
INSERT INTO Integrantes (matricula, nombre, apellido, url) VALUES
('UG0507', 'Marco', 'Ortigoza', 'integrante/UG0507'),
('Y12858', 'Elvio', 'Martinez', 'integrante/Y12858'),
('Y17825', 'Alexis', 'Duarte', 'integrante/Y17825'),
('Y23865', 'Gabriel', 'Garcete', 'integrante/Y23865');

SELECT * FROM integrantes
-- INSERT DEL TIPOMEDIA

INSERT INTO TipoMedia (id_tipo_media, nombre) VALUES
(1, 'Imagen'),
(2, 'Youtube'),
(3, 'Dibujo');

SELECT * FROM TipoMedia

-- INSERT DE LOS DATOS DE MEDIA DE LOS INTEGRANTES
INSERT INTO Media (id, titulo, nombre, url, matricula, id_tipo_media) VALUES 
(1, 'Video Favorito de Youtube', 'Youtube', 'https://www.youtube.com/embed/b8-tXG8KrWs?si=qBWFL28iK9JTU3JZ', 'Y26426', 2),
(2, 'Imagen que me representa', 'imagen', '/assets/images/imagen-56.jpeg', 'Y26426', 1),
(3, 'Dibujo en Paint', 'dibujo', '../../assets/images/imagen-57.png', 'Y26426', 3),

(4, 'Video Favorito de Youtube', 'Youtube', 'https://www.youtube.com/embed/RW75cGvO5xY', 'UG0507', 2),
(5, 'Imagen representativa', 'Imagen', '/assets/images/saturno.jpg', 'UG0507', 1),
(6, 'terere', 'Dibujo', '../../assets/images/TERERE.png', 'UG0507', 3),

(7, 'Video favorito de youtube', 'Youtube', 'https://www.youtube.com/embed/VhoHnKuf-HI', 'Y12858', 2),
(8, 'Imagen representativa', 'Imagen', '/assets/images/melissa.jpg', 'Y12858', 1),
(9, 'Dibujo en paint', 'Dibujo', '../../assets/images/dibujo-Elvio.png', 'Y12858', 3),

(10, 'Video favorito de youtube', 'Youtube', 'https://www.youtube.com/embed/U1ivmi3_IeI?si=9QjM1bJzS3uUpMYk', 'Y17825', 2),
(11, 'Imagen que representa mi personalidad.', 'Imagen', '/assets/images/imagen-personalidad.jpg', 'Y17825', 1),
(12, 'Dibujo en paint', 'Dibujo', '../../assets/images/dibujo-sistema-solar.png', 'Y17825', 3),

(13, 'Video favorito de youtube', 'Youtube', 'https://www.youtube.com/embed/B4LvDiIi128?rel=0', 'Y23865', 2),
(14, 'Imagen representativa', 'Imagen', '/assets/images/messi_pou.jpeg', 'Y23865', 1),
(15, 'Dibujo en paint', 'Dibujo', '../../assets/images/paint_garcete.jpg', 'Y23865', 3);

SELECT * FROM Media