const express = require('express');
const hbs = require('hbs');
const app = express();
require("dotenv").config();


// Importamos archivos de rutas
const router = require("./routes/public");
// Usamos los archivos de rutas
app.use("/", router);

//configuracion para motor de vistas hbs
app.use(express.static('public'));
app.set('view engine', 'hbs');
app.set('views', __dirname + '/views');
hbs.registerPartials(__dirname + "/views/partials");

const puerto = process.env.PORT || 3000;

app.listen(puerto, () => {
    console.log(`El servidor se está ejecutando en http://localhost:${puerto}`);
});


